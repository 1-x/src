typedef enum
{
	PeakDetection_State,
	TimeDiff_Calc_State,
	Moving_Avg_State,
	BPM_Calc_State,
	Display_State,
} eSystemState;

void Peakhandler()
{
	typedef enum {false, true} bool;

	int SIGNAL[SIGNAL_SIZE_IN_SECS * SAMPLING_RATE];
	int SIGNAL_LENGTH = -1;

	int R_PEAKS[SIGNAL_SIZE_IN_SECS*10];
	int R_PEAKS_LENGTH = 0;
	int SAMPLE_COUNT = 0;

	void step1_readSignal(char signal_filename[]) {
	  printf("Reading Signal from file: %s\n", signal_filename);
	  FILE *fin = fopen(signal_filename, "r");
	  int data = -99999;
	  int i = 0;
	  while (!feof(fin) && i < SIGNAL_SIZE_IN_SECS * SAMPLING_RATE) {
	    fscanf(fin, "%d", &data);
	    SIGNAL[i] = data;
	    i++;
	  }
	  SIGNAL_LENGTH = i;
	  printf("Finished reading Signal. Total number of samples = %d\n", SIGNAL_LENGTH);
	}

	void output(int v){
	  // printf("%d\n", v);
	  if (v) {
	    R_PEAKS_LENGTH++;
	    R_PEAKS[R_PEAKS_LENGTH] = SAMPLE_COUNT;
	  }
	  SAMPLE_COUNT++;
	}

	int * step2_filtering(){

	  int * dcblock = (int*) calloc (SIGNAL_LENGTH, sizeof(int));
	  int * lowpass = (int*) calloc (SIGNAL_LENGTH, sizeof(int));
	  int * highpass = (int*) calloc (SIGNAL_LENGTH, sizeof(int));

	  for(int i=0; i<SIGNAL_LENGTH; i++){
	    // DC Block filter
	    // This was not proposed on the original paper.
	    // It is not necessary and can be removed if your sensor or database has no DC noise.
	    if (i >= 1)
	      dcblock[i] = SIGNAL[i] - SIGNAL[i-1] + 0.995*dcblock[i-1];
	    else
	      dcblock[i] = 0;

	    // Low Pass filter
	    // Implemented as proposed by the original paper:  y(nT) = 2y(nT - T) - y(nT - 2T) + x(nT) - 2x(nT - 6T) + x(nT - 12T)
	    lowpass[i] = dcblock[i];
	    if (i >= 1)
	      lowpass[i] += 2*lowpass[i-1];
	    if (i >= 2)
	      lowpass[i] -= lowpass[i-2];
	    if (i >= 6)
	      lowpass[i] -= 2*dcblock[i-6];
	    if (i >= 12)
	      lowpass[i] += dcblock[i-12];
	// printf("%d\n", lowpass[i]);
	    // High Pass filter
	    // Implemented as proposed by the original paper:  y(nT) = 32x(nT - 16T) - [y(nT - T) + x(nT) - x(nT - 32T)]
	    highpass[i] = -lowpass[i];
	    if (i >= 1)
	      highpass[i] -= highpass[i-1];
	    if (i >= 16)
	      highpass[i] += 32*lowpass[i-16];
	    if (i >= 32)
	      highpass[i] += lowpass[i-32];
	    // printf("%d\n", highpass[i]);
	  }

	  free(lowpass);
	  return highpass;
	}


	void step3_segmenter(int * signalFiltered){

	  int highpass[BUFFSIZE], derivative[BUFFSIZE], squared[BUFFSIZE], integral[BUFFSIZE], outputSignal[BUFFSIZE];

	  // for(int i=0; i<SIGNAL_LENGTH; i++){ printf("%d\n", signalFiltered[i]); }

	  int rr1[8], rr2[8], rravg1, rravg2, rrlow = 0, rrhigh = 0, rrmiss = 0;
	  int i, j, lastQRS = 0, lastSlope = 0;
	  int peak_i = 0, peak_f = 0, threshold_i1 = 0, threshold_i2 = 0, threshold_f1 = 0, threshold_f2 = 0, spk_i = 0, spk_f = 0, npk_i = 0, npk_f = 0;
	  bool qrs, regular = true, prevRegular;
	  int current; // signal buffers index. If the buffers still aren't completely filled, it shows the last filled position. Once the buffers are full, it'll always show the last position, and new samples will make the buffers shift, discarding the oldest sample and storing the newest one on the last position.


	  // Initializing the RR averages
	  for (i = 0; i < 8; i++) {
	    rr1[i] = 0;
	    rr2[i] = 0;
	  }



	  int FS = SAMPLING_RATE;
	  int DELAY = FILTER_DELAY_IN_SAMPLES;

	  int sample = 0;
	  for(sample = 0; sample < SIGNAL_LENGTH; sample++){
	    // Test if the buffers are full.
	    // If they are, shift them, discarding the oldest sample and adding the new one at the end.
	    // Else, just put the newest sample in the next free position.
	    // Update 'current' so that the program knows where's the newest sample.
	    if (sample >= BUFFSIZE) {
	      for (i = 0; i < BUFFSIZE - 1; i++) {
	        highpass[i] = highpass[i+1];
	        derivative[i] = derivative[i+1];
	        squared[i] = squared[i+1];
	        integral[i] = integral[i+1];
	        outputSignal[i] = outputSignal[i+1];
	      }
	      current = BUFFSIZE - 1;
	    } else {
	      current = sample;
	    }
	    highpass[current] = signalFiltered[sample];



	    // -------------------------------------
	    // -- DERIVATIVE


	    // Derivative filter
	    // This is an alternative implementation, the central difference method.
	    // f'(a) = [f(a+h) - f(a-h)]/2h
	    // The original formula used by Pan-Tompkins was:
	    // y(nT) = (1/8T)[-x(nT - 2T) - 2x(nT - T) + 2x(nT + T) + x(nT + 2T)]
	    derivative[current] = highpass[current];
	    if (current > 0)
	      derivative[current] -= highpass[current-1];


	    // -------------------------------------
	    // -- SQUARING


	    // This just squares the derivative, to get rid of negative values and emphasize high frequencies.
	    // y(nT) = [x(nT)]^2.
	    squared[current] = derivative[current]*derivative[current];




	    // -------------------------------------
	    // -- INTEGRATION


	    // Moving-Window Integration
	    // Implemented as proposed by the original paper.
	    // y(nT) = (1/N)[x(nT - (N - 1)T) + x(nT - (N - 2)T) + ... x(nT)]
	    // WINDOWSIZE, in samples, must be defined so that the window is ~150ms.
	    // int WINDOWSIZE = (int) (0.15 * SAMPLING_RATE);
	    int WINDOWSIZE = 20;
	    integral[current] = 0;
	    for (i = 0; i < WINDOWSIZE; i++)
	    {
	      if (current >= (int)i)
	        integral[current] += highpass[current - i];
	      else
	        break;
	    }
	    integral[current] /= (int)i;





	    // -------------------------------------
	    // -- PEAK DETECTION

	    qrs = false;

	    // If the current signal is above one of the thresholds (integral or filtered signal), it's a peak candidate.
	        if (integral[current] >= threshold_i1 || highpass[current] >= threshold_f1)
	        {
	            peak_i = integral[current];
	            peak_f = highpass[current];
	        }

	    // If both the integral and the signal are above their thresholds, they're probably signal peaks.
	    if ((integral[current] >= threshold_i1) && (highpass[current] >= threshold_f1))
	    {
	      // There's a 200ms latency. If the new peak respects this condition, we can keep testing.
	      if (sample > lastQRS + 0.2*FS)
	      {
	          // If it respects the 200ms latency, but it doesn't respect the 360ms latency, we check the slope.
	          if (sample <= lastQRS + 0.36*FS)
	          {
	              if (squared[current] < (int)(lastSlope/2))
	                      {
	                          qrs = false;
	                      }

	                      else
	                      {
	                          spk_i = 0.125*peak_i + 0.875*spk_i;
	                          threshold_i1 = npk_i + 0.25*(spk_i - npk_i);
	                          threshold_i2 = 0.5*threshold_i1;

	                          spk_f = 0.125*peak_f + 0.875*spk_f;
	                          threshold_f1 = npk_f + 0.25*(spk_f - npk_f);
	                          threshold_f2 = 0.5*threshold_f1;

	                          lastSlope = squared[current];
	                          qrs = true;
	                      }
	          }
	          // If it was above both thresholds and respects both latency periods, it certainly is a R peak.
	          else
	          {
	              if (squared[current] > (int)(lastSlope/2))
	                      {
	                          spk_i = 0.125*peak_i + 0.875*spk_i;
	                          threshold_i1 = npk_i + 0.25*(spk_i - npk_i);
	                          threshold_i2 = 0.5*threshold_i1;

	                          spk_f = 0.125*peak_f + 0.875*spk_f;
	                          threshold_f1 = npk_f + 0.25*(spk_f - npk_f);
	                          threshold_f2 = 0.5*threshold_f1;

	                          lastSlope = squared[current];
	                          qrs = true;
	                      }
	          }
	      }
	      // If the new peak doesn't respect the 200ms latency, it's noise. Update thresholds and move on to the next sample.
	      else
	            {
	                peak_i = integral[current];
	                npk_i = 0.125*peak_i + 0.875*npk_i;
	                threshold_i1 = npk_i + 0.25*(spk_i - npk_i);
	                threshold_i2 = 0.5*threshold_i1;
	                peak_f = highpass[current];
	                npk_f = 0.125*peak_f + 0.875*npk_f;
	                threshold_f1 = npk_f + 0.25*(spk_f - npk_f);
	                threshold_f2 = 0.5*threshold_f1;
	                qrs = false;
	                outputSignal[current] = qrs;
	                if (sample > DELAY + BUFFSIZE){
	                          output(outputSignal[0]);
	                        }
	                continue;
	            }

	    }

	    // If a R-peak was detected, the RR-averages must be updated.
	    if (qrs)
	    {

	      // Add the newest RR-interval to the buffer and get the new average.
	      rravg1 = 0;
	      for (i = 0; i < 7; i++)
	      {
	        rr1[i] = rr1[i+1];
	        rravg1 += rr1[i];
	      }
	      rr1[7] = sample - lastQRS;
	      lastQRS = sample;
	      rravg1 += rr1[7];
	      rravg1 *= 0.125;

	      // If the newly-discovered RR-average is normal, add it to the "normal" buffer and get the new "normal" average.
	      // Update the "normal" beat parameters.
	      if ( (rr1[7] >= rrlow) && (rr1[7] <= rrhigh) )
	      {
	        rravg2 = 0;
	        for (i = 0; i < 7; i++)
	        {
	          rr2[i] = rr2[i+1];
	          rravg2 += rr2[i];
	        }
	        rr2[7] = rr1[7];
	        rravg2 += rr2[7];
	        rravg2 *= 0.125;
	        rrlow = 0.92*rravg2;
	        rrhigh = 1.16*rravg2;
	        rrmiss = 1.66*rravg2;
	      }

	      prevRegular = regular;
	      if (rravg1 == rravg2)
	      {
	        regular = true;
	      }
	      // If the beat had been normal but turned odd, change the thresholds.
	      else
	      {
	        regular = false;
	        if (prevRegular)
	        {
	          threshold_i1 /= 2;
	          threshold_f1 /= 2;
	        }
	      }
	    }
	    // If no R-peak was detected, it's important to check how long it's been since the last detection.
	    else
	    {
	        // If no R-peak was detected for too long, use the lighter thresholds and do a back search.
	      // However, the back search must respect the 200ms limit.
	      if ((sample - lastQRS > (long unsigned int)rrmiss) && (sample > lastQRS + 0.2*FS))
	      {
	        for (i = current - (sample - lastQRS) + 0.2*FS; i < (long unsigned int)current; i++)
	        {
	          if ( (integral[i] > threshold_i2) && (highpass[i] > threshold_f2))
	          {
	            peak_i = integral[i];
	            peak_f = highpass[i];
	            spk_i = 0.25*peak_i+ 0.75*spk_i;
	            spk_f = 0.25*peak_f + 0.75*spk_f;
	            threshold_i1 = npk_i + 0.25*(spk_i - npk_i);
	            threshold_i2 = 0.5*threshold_i1;
	            lastSlope = squared[i];
	            threshold_f1 = npk_f + 0.25*(spk_f - npk_f);
	            threshold_f2 = 0.5*threshold_f1;
	            // If a signal peak was detected on the back search, the RR attributes must be updated.
	            // This is the same thing done when a peak is detected on the first try.
	            //RR Average 1
	            rravg1 = 0;
	            for (j = 0; j < 7; j++)
	            {
	              rr1[j] = rr1[j+1];
	              rravg1 += rr1[j];
	            }
	            rr1[7] = sample - i - lastQRS;
	            lastQRS = sample - i;
	            outputSignal[current-lastQRS] = true;
	            rravg1 += rr1[7];
	            rravg1 *= 0.125;

	            //RR Average 2
	            if ( (rr1[7] >= rrlow) && (rr1[7] <= rrhigh) )
	            {
	              rravg2 = 0;
	              for (i = 0; i < 7; i++)
	              {
	                rr2[i] = rr2[i+1];
	                rravg2 += rr2[i];
	              }
	              rr2[7] = rr1[7];
	              rravg2 += rr2[7];
	              rravg2 *= 0.125;
	              rrlow = 0.92*rravg2;
	              rrhigh = 1.16*rravg2;
	              rrmiss = 1.66*rravg2;
	            }

	            prevRegular = regular;
	            if (rravg1 == rravg2)
	            {
	              regular = true;
	            }
	            else
	            {
	              regular = false;
	              if (prevRegular)
	              {
	                threshold_i1 /= 2;
	                threshold_f1 /= 2;
	              }
	            }

	            break;
	          }
	        }
	      }
	      // Definitely no signal peak was detected.
	      if (!qrs)
	      {
	        // If some kind of peak had been detected, then it's certainly a noise peak. Thresholds must be updated accordinly.
	        if ((integral[current] >= threshold_i1) || (highpass[current] >= threshold_f1))
	        {
	          peak_i = integral[current];
	          npk_i = 0.125*peak_i + 0.875*npk_i;
	          threshold_i1 = npk_i + 0.25*(spk_i - npk_i);
	          threshold_i2 = 0.5*threshold_i1;
	          peak_f = highpass[current];
	          npk_f = 0.125*peak_f + 0.875*npk_f;
	          threshold_f1 = npk_f + 0.25*(spk_f - npk_f);
	          threshold_f2 = 0.5*threshold_f1;
	        }
	      }
	    }
	    // The current implementation outputs '0' for every sample where no peak was detected,
	    // and '1' for every sample where a peak was detected. It should be changed to fit
	    // the desired application.
	    // The 'if' accounts for the delay introduced by the filters: we only start outputting after the delay.
	    // However, it updates a few samples back from the buffer. The reason is that if we update the detection
	    // for the current sample, we might miss a peak that could've been found later by backsearching using
	    // lighter thresholds. The final waveform output does match the original signal, though.
	    outputSignal[current] = qrs;
	    if (sample > DELAY + BUFFSIZE){
	      output(outputSignal[0]);
	      }

	  }

	  // Output the last remaining samples on the buffer
	  for (i = 1; i < BUFFSIZE; i++)
	    output(outputSignal[i]);

	}

	void step4_features_extraction(){

	    int numTemplates = 0;
	    float snP_x=0, snP_y=0, snQ_x=0, snQ_y=0, snR_x=0, snR_y=0, snS_x=0, snS_y=0, snT_x=0, snT_y=0;


	    for (int i=0; i<R_PEAKS_LENGTH;i++){
	        if (R_PEAKS[i] < 2 * SAMPLING_RATE) // ignoring the first 2 seconds
	          continue;

	        int R_x = R_PEAKS[i];


	        //improving R_x
	        int tentativeRx = R_x;
	        int c;
	        for(c=R_x; SIGNAL[c] >= SIGNAL[tentativeRx]; c++){
	          tentativeRx = c;
	        }
	        for(c=R_x; SIGNAL[c] >= SIGNAL[tentativeRx]; c--){
	          tentativeRx = c;
	        }
	        R_x = tentativeRx;


	        int start = R_x - 0.2 * SAMPLING_RATE;
	        int end = R_x + 0.4 * SAMPLING_RATE;


	        // finding P, Q
	        int Q_x = -1;
	        int P_x = -1;
	        for (int c = R_x-1; c>= start; c--){
	          if (Q_x == -1)
	            if (SIGNAL[c] < SIGNAL[c-1])
	                Q_x = c;
	        }
	        if (Q_x == -1){
	          printf("Q point not found!");
	        } else {
	          for (int c = Q_x-1; c>= start; c--){
	            if (SIGNAL[c] > SIGNAL[P_x])
	              P_x = c;
	          }
	        }


	        // finding S, T
	        int S_x = -1;
	        int T_x = -1;
	        for (int c = R_x+1; c<= end; c++){
	          if (S_x == -1)
	            if (SIGNAL[c] > SIGNAL[c-1])
	                S_x = c-1;
	        }
	        if (S_x == -1){
	          printf("S point not found!");
	        } else {
	          for (int c = S_x+1; c<= end; c++){
	            if (SIGNAL[c] > SIGNAL[T_x])
	              T_x = c;
	          }
	        }

	        // printf("Cycle #%d: P=%d, Q=%d, R=%d, S=%d, T=%d, start=%d, end=%d\n", i, P_x, Q_x, R_x, S_x, T_x, start, end);


	        if (P_x > 0 && Q_x > 0 && R_x > 0 && S_x > 0 && T_x > 0){ //cycle has all points identified
	          int P_y = SIGNAL[P_x], Q_y = SIGNAL[Q_x], R_y = SIGNAL[R_x], S_y = SIGNAL[S_x], T_y = SIGNAL[T_x];

	          // normalizing values
	          float nP_x = 1.0 * (P_x - start) / (end - start);
	          float nQ_x = 1.0 * (Q_x - start) / (end - start);
	          float nR_x = 1.0 * (R_x - start) / (end - start);
	          float nS_x = 1.0 * (S_x - start) / (end - start);
	          float nT_x = 1.0 * (T_x - start) / (end - start);

	          int max =  R_y;
	          if (P_y > max) max =  P_y;
	          if (T_y > max) max =  T_y;

	          int min = Q_y;
	          if (S_y < min)
	            min = S_y;

	          float nP_y = 1.0 * (P_y - min) / (max - min);
	          float nQ_y = 1.0 * (Q_y - min) / (max - min);
	          float nR_y = 1.0 * (R_y - min) / (max - min);
	          float nS_y = 1.0 * (S_y - min) / (max - min);
	          float nT_y = 1.0 * (T_y - min) / (max - min);

	          // float tmplt[10] =  { nP_x, nQ_x, nR_x, nS_x, nT_x, nP_y, nQ_y, nR_y, nS_y, nT_y };
	          // printf("Template= (%f, %f, %f, %f, %f, %f, %f, %f, %f, %f)\n\n", nP_x, nQ_x, nR_x, nS_x, nT_x, nP_y, nQ_y, nR_y, nS_y, nT_y);
	          numTemplates++;
	          snP_x += nP_x;
	          snP_y += nP_y;
	          snQ_x += nQ_x;
	          snQ_y += nQ_y;
	          snR_x += nR_x;
	          snR_y += nR_y;
	          snS_x += nS_x;
	          snS_y += nS_y;
	          snT_x += nT_x;
	          snT_y += nT_y;
	        }
	    }

	    float fP_x = snP_x / numTemplates;
	    float fP_y = snP_y / numTemplates;
	    float fQ_x = snQ_x / numTemplates;
	    float fQ_y = snQ_y / numTemplates;
	    float fR_x = snR_x / numTemplates;
	    float fR_y = snR_y / numTemplates;
	    float fS_x = snS_x / numTemplates;
	    float fS_y = snS_y / numTemplates;
	    float fT_x = snT_x / numTemplates;
	    float fT_y = snT_y / numTemplates;


	    //printf("User Template= {%f, %f, %f, %f, %f, %f, %f, %f, %f, %f}\n\n", fP_x, fQ_x, fR_x, fS_x, fT_x, fP_y, fQ_y, fR_y, fS_y, fT_y);
	    USER_TEMPLATE[0] = fP_x;
	    USER_TEMPLATE[1] = fQ_x;
	    USER_TEMPLATE[2] = fR_x;
	    USER_TEMPLATE[3] = fS_x;
	    USER_TEMPLATE[4] = fT_x;
	    USER_TEMPLATE[5] = fP_y;
	    USER_TEMPLATE[6] = fQ_y;
	    USER_TEMPLATE[7] = fR_y;
	    USER_TEMPLATE[8] = fS_y;
	    USER_TEMPLATE[9] = fT_y;
	}

	void step5_matching(){
	  // printf("step5_matching - USER_TEMPLATE[0]=%f\n", USER_TEMPLATE[0]);
	  // printf("step5_matching - LIST_TEMPLATES[0][0]=%f\n", LIST_TEMPLATES[0][0]);
	  int userId = -1;
	  int prevUserId =-1;
	  double preMinDist = 99999;
	  double minDist = 99999;
	  for(int i=0; i<LIST_TEMPLATES_LENGTH; i++){
	    // printf("Testing userId %d\n", i);
	    float * t = LIST_TEMPLATES[i];
	    double s = 0;
	    for(int k=0; k<10;k++){
	      s = s + ( (t[k] - USER_TEMPLATE[k]) * (t[k] - USER_TEMPLATE[k]));
	      // printf("t[%d] =%f  | User-templte(k)=%f\n", k, t[k], USER_TEMPLATE[k]);
	    }

	    double dist = sqrt((double) s);
	    if (dist < minDist) {
	      preMinDist = minDist;
	      prevUserId = userId;
	      minDist = dist;
	      userId = i;
	    }
	  }

	  //printf("Closest to Template is userId %d with dist=%f\n", userId, (float) minDist);
	  //printf("Second to Closest to Template is userId %d with dist=%f\n", prevUserId, (float) preMinDist);

	}

	// - - - - - - - - - - - - - - - - - -
	// TEMPLATES PRE-REGISTERED

	float template_user_0[10] = {0.103908, 0.300230, 0.333333, 0.358621, 0.605977, 0.318059, 0.000000, 0.990012, 0.253358, 0.447830};
	float template_user_1[10] = {0.221333, 0.296485, 0.333333, 0.353818, 0.574788, 0.175081, 0.043954, 0.637867, 0.181294, 0.746571};
	float template_user_2[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_3[10] = {0.199333, 0.305000, 0.333333, 0.366333, 0.633167, 0.367105, 0.004237, 0.827265, 0.133950, 0.758213};
	float template_user_4[10] = {0.117778, 0.300000, 0.333333, 0.363590, 0.646496, 0.414064, 0.107539, 0.939495, 0.066901, 0.592610};
	float template_user_5[10] = {0.198374, 0.304065, 0.333333, 0.359675, 0.625691, 0.227508, 0.000421, 0.762014, 0.213194, 0.730991};
	float template_user_6[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_7[10] = {0.209333, 0.318286, 0.333333, 0.346667, 0.388000, 0.543526, 0.000000, 0.650840, 0.230488, 0.976753};
	float template_user_8[10] = {0.225741, 0.298889, 0.333333, 0.356667, 0.620000, 0.196782, 0.000758, 0.816756, 0.284369, 0.677528};
	float template_user_9[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_10[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_11[10] = {0.244394, 0.307879, 0.333333, 0.355151, 0.533939, 0.231599, 0.003740, 0.650445, 0.165417, 0.847854};
	float template_user_12[10] = {0.202222, 0.282963, 0.333333, 0.357778, 0.509481, 0.148428, 0.037581, 0.829346, 0.391297, 0.803877};
	float template_user_13[10] = {0.273651, 0.281270, 0.333333, 0.342540, 0.399683, 0.065523, 0.000000, 0.518979, 0.467085, 0.996831};
	float template_user_14[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_15[10] = {0.117167, 0.311667, 0.333333, 0.355500, 0.526167, 0.550989, 0.022183, 0.521706, 0.120590, 0.630926};
	float template_user_16[10] = {0.140667, 0.283833, 0.333333, 0.372500, 0.643000, 0.166277, 0.056187, 0.973157, 0.204116, 0.470603};
	float template_user_17[10] = {0.166944, 0.263333, 0.333333, 0.376528, 0.591667, 0.170949, 0.069291, 0.914173, 0.286938, 0.538947};
	float template_user_18[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_19[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_20[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_21[10] = {0.239589, 0.307407, 0.333333, 0.357531, 0.698930, 0.444761, 0.069493, 0.452650, 0.107414, 0.968766};
	float template_user_22[10] = {0.124902, 0.256667, 0.333333, 0.411765, 0.663725, 0.180170, 0.005734, 0.994447, 0.400489, 0.670752};
	float template_user_23[10] = {0.191667, 0.310278, 0.333333, 0.349167, 0.582222, 0.394329, 0.047281, 0.248459, 0.062338, 0.711898};
	float template_user_24[10] = {0.209722, 0.232222, 0.333333, 0.393056, 0.642222, 0.176871, 0.132820, 0.888524, 0.348892, 0.660422};
	float template_user_25[10] = {0.040000, 0.270877, 0.333333, 0.393684, 0.792632, 0.206813, 0.022256, 0.978902, 0.139153, 0.408806};
	float template_user_26[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_27[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_28[10] = {0.175914, 0.289677, 0.333333, 0.357204, 0.588602, 0.401329, 0.066049, 0.336099, 0.110733, 0.746621};
	float template_user_29[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_30[10] = {0.114167, 0.262500, 0.333333, 0.380000, 0.751667, 0.296299, 0.214570, 1.000000, 0.000000, 0.547958};
	float template_user_31[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_32[10] = {0.039524, 0.284762, 0.333333, 0.384762, 0.795714, 0.197838, 0.096304, 1.000000, 0.000000, 0.551154};
	float template_user_33[10] = {0.079130, 0.314493, 0.333333, 0.345217, 0.437681, 0.723843, 0.004527, 0.280223, 0.183721, 0.663098};
	float template_user_34[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_35[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_36[10] = {0.053333, 0.285833, 0.333333, 0.370833, 0.809167, 0.247115, 0.000000, 1.000000, 0.145327, 0.427918};
	float template_user_37[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_38[10] = {9,9,9,9,9,9,9,9,9,9};
	float template_user_39[10] = {9,9,9,9,9,9,9,9,9,9};



	void step0_loadTemplates(){
	    LIST_TEMPLATES[0] = template_user_0;
	    LIST_TEMPLATES[1] = template_user_1;
	    LIST_TEMPLATES[2] = template_user_2;
	    LIST_TEMPLATES[3] = template_user_3;
	    LIST_TEMPLATES[4] = template_user_4;
	    LIST_TEMPLATES[5] = template_user_5;
	    LIST_TEMPLATES[6] = template_user_6;
	    LIST_TEMPLATES[7] = template_user_7;
	    LIST_TEMPLATES[8] = template_user_8;
	    LIST_TEMPLATES[9] = template_user_9;
	    LIST_TEMPLATES[10] = template_user_10;
	    LIST_TEMPLATES[11] = template_user_11;
	    LIST_TEMPLATES[12] = template_user_12;
	    LIST_TEMPLATES[13] = template_user_13;
	    LIST_TEMPLATES[14] = template_user_14;
	    LIST_TEMPLATES[15] = template_user_15;
	    LIST_TEMPLATES[16] = template_user_16;
	    LIST_TEMPLATES[17] = template_user_17;
	    LIST_TEMPLATES[18] = template_user_18;
	    LIST_TEMPLATES[19] = template_user_19;
	    LIST_TEMPLATES[20] = template_user_20;
	    LIST_TEMPLATES[21] = template_user_21;
	    LIST_TEMPLATES[22] = template_user_22;
	    LIST_TEMPLATES[23] = template_user_23;
	    LIST_TEMPLATES[24] = template_user_24;
	    LIST_TEMPLATES[25] = template_user_25;
	    LIST_TEMPLATES[26] = template_user_26;
	    LIST_TEMPLATES[27] = template_user_27;
	    LIST_TEMPLATES[28] = template_user_28;
	    LIST_TEMPLATES[29] = template_user_29;
	    LIST_TEMPLATES[30] = template_user_30;
	    LIST_TEMPLATES[31] = template_user_31;
	    LIST_TEMPLATES[32] = template_user_32;
	    LIST_TEMPLATES[33] = template_user_33;
	    LIST_TEMPLATES[34] = template_user_34;
	    LIST_TEMPLATES[35] = template_user_35;
	    LIST_TEMPLATES[36] = template_user_36;
	    LIST_TEMPLATES[37] = template_user_37;
	    LIST_TEMPLATES[38] = template_user_38;
	    LIST_TEMPLATES[39] = template_user_39;



	    LIST_TEMPLATES_LENGTH = 40;
	    // printf("step0_loading - LIST_TEMPLATES[0][0]=%f\n", LIST_TEMPLATES[0][0]);
	    // printf("%f", LIST_TEMPLATES[1][1]);
	    // printf("%lu", sizeof(LIST_TEMPLATES)/sizeof(LIST_TEMPLATES[0]));
	}





	int main() {

	  /* my first program in C */
	  printf("Starting ECG Authenticator... \n\n");



	  step0_loadTemplates();


	  // step1_readSignal("ecg_for_template_generation/ecg_user_3.txt");
	  //step1_readSignal("ecg_for_testing/ecg_user_1_test.txt");
	  step1_readSignal("/scratch/dhruvgajaria/gem5_ret_aware/ecg_code/ecg_for_testing/ecg_user_1_test.txt");
	  int * signalFiltered = step2_filtering(); // Block 1

	  step3_segmenter(signalFiltered);  // Block 2

	  step4_features_extraction();  // Block 3

	  step5_matching(); // Block 4


	  // for(int i=0; i<SIGNAL_LENGTH; i++){ printf("%d\n", signalFiltered[i]); }
	  // for(int i=0; i<R_PEAKS_LENGTH; i++){ printf("%d\n", R_PEAKS[i]); }
	  printf("Done! \n");
	  return 0;
	}





	/*


	//TESTING REPORT

	userId -> Result
	0 -> N/A (Segmenation fault)
	1 -> Correct
	2 -> N/A  (NOT REGISTERED DUE TO SEG FAULT)
	3 -> Correct
	4 -> Correct
	5 -> Correct
	6 -> N/A  (NOT REGISTERED DUE TO SEG FAULT)
	7 -> Correct
	8 -> Correct
	9 -> N/A (Segmenation fault)
	10 -> N/A  (NOT REGISTERED DUE TO SEG FAULT)
	11 -> Correct
	12 -> Correct
	13 -> Correct
	14 -> N/A  (NOT REGISTERED DUE TO SEG FAULT)
	15 -> Correct
	16 -> Correct
	17 -> Wrong (11)
	18 -> N/A  (NOT REGISTERED DUE TO SEG FAULT)
	19 -> N/A (Segmenation fault)

	Acc: 11 / 12 = 91.7%

	*/
	/* USER CODE END Includes */

	/* Private typedef -----------------------------------------------------------*/
	/* USER CODE BEGIN PTD */

	/* USER CODE END PTD */

	/* Private define ------------------------------------------------------------*/
	/* USER CODE BEGIN PD */
	/* USER CODE END PD */

	/* Private macro -------------------------------------------------------------*/
	/* USER CODE BEGIN PM */

	/* USER CODE END PM */

	/* Private variables ---------------------------------------------------------*/
	 UART_HandleTypeDef huart2;

	/* USER CODE BEGIN PV */

	/* USER CODE END PV */

	/* Private function prototypes -----------------------------------------------*/
	void SystemClock_Config(void);
	static void MX_GPIO_Init(void);
	static void MX_USART2_UART_Init(void);

	float USER_TEMPLATE[10]; // holds the Autenthication Template generated by the current ECG reading;

	float * LIST_TEMPLATES[40]; // the database of templates. Code will compare USER_TEMPLATE against each of the templates in LIST_TEMPLATES
	int LIST_TEMPLATES_LENGTH;
	/* USER CODE BEGIN PFP */

	/* USER CODE END PFP */

	/* Private user code ---------------------------------------------------------*/
	/* USER CODE BEGIN 0 */

	/* USER CODE END 0 */

	/**
	  * @brief  The application entry point.
	  * @retval int
	  */
	int main(void)
	{
	  /* USER CODE BEGIN 1 */

	  /* USER CODE END 1 */

	  /* MCU Configuration--------------------------------------------------------*/

	  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	  HAL_Init();

	  /* USER CODE BEGIN Init */

	  /* USER CODE END Init */

	  /* Configure the system clock */
	  SystemClock_Config();

	  /* USER CODE BEGIN SysInit */

	  /* USER CODE END SysInit */

	  /* Initialize all configured peripherals */
	  MX_GPIO_Init();
	  MX_USART2_UART_Init();
	  /* USER CODE BEGIN 2 */

	  /* USER CODE END 2 */

	  /* Infinite loop */
	  /* USER CODE BEGIN WHILE */
	  while (1)
	  {

	    /* USER CODE END WHILE */

	    /* USER CODE BEGIN 3 */
	  }
	  /* USER CODE END 3 */
	}

	/**
	  * @brief System Clock Configuration
	  * @retval None
	  */
	void SystemClock_Config(void)
	{
	  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	  /** Initializes the RCC Oscillators according to the specified parameters
	  * in the RCC_OscInitTypeDef structure.
	  */
	  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  /** Initializes the CPU, AHB and APB buses clocks
	  */
	  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
	                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	  {
	    Error_Handler();
	  }
	}

	/**
	  * @brief USART2 Initialization Function
	  * @param None
	  * @retval None
	  */
	static void MX_USART2_UART_Init(void)
	{

	  /* USER CODE BEGIN USART2_Init 0 */

	  /* USER CODE END USART2_Init 0 */

	  /* USER CODE BEGIN USART2_Init 1 */

	  /* USER CODE END USART2_Init 1 */
	  huart2.Instance = USART2;
	  huart2.Init.BaudRate = 38400;
	  huart2.Init.WordLength = UART_WORDLENGTH_8B;
	  huart2.Init.StopBits = UART_STOPBITS_1;
	  huart2.Init.Parity = UART_PARITY_NONE;
	  huart2.Init.Mode = UART_MODE_TX_RX;
	  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  if (HAL_UART_Init(&huart2) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  /* USER CODE BEGIN USART2_Init 2 */

	  /* USER CODE END USART2_Init 2 */

	}

	/**
	  * @brief GPIO Initialization Function
	  * @param None
	  * @retval None
	  */
	static void MX_GPIO_Init(void)
	{
	  GPIO_InitTypeDef GPIO_InitStruct = {0};

	  /* GPIO Ports Clock Enable */
	  __HAL_RCC_GPIOF_CLK_ENABLE();
	  __HAL_RCC_GPIOA_CLK_ENABLE();
	  __HAL_RCC_GPIOB_CLK_ENABLE();

	  /*Configure GPIO pin Output Level */
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);

	  /*Configure GPIO pin : PB4 */
	  GPIO_InitStruct.Pin = GPIO_PIN_4;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	  /*Configure GPIO pin : PB5 */
	  GPIO_InitStruct.Pin = GPIO_PIN_5;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	}

	/* USER CODE BEGIN 4 */

	/* USER CODE END 4 */

	/**
	  * @brief  This function is executed in case of error occurrence.
	  * @retval None
	  */
	void Error_Handler(void)
	{
	  /* USER CODE BEGIN Error_Handler_Debug */
	  /* User can add his own implementation to report the HAL error return state */
	  __disable_irq();
	  while (1)
	  {
	  }
	  /* USER CODE END Error_Handler_Debug */
	}

	#ifdef  USE_FULL_ASSERT
	/**
	  * @brief  Reports the name of the source file and the source line number
	  *         where the assert_param error has occurred.
	  * @param  file: pointer to the source file name
	  * @param  line: assert_param error line source number
	  * @retval None
	  */
	void assert_failed(uint8_t *file, uint32_t line)
	{
	  /* USER CODE BEGIN 6 */
	  /* User can add his own implementation to report the file name and line number,
	     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	  /* USER CODE END 6 */
	}
	#endif 
	
}


uint32_t TimeDiffhandler(uint32_t* old_t, uint32_t* new_t )
{
    uint32_t deltaT = 0;
	*new_t = HAL_GetTick();
	deltaT = *new_t - *old_t;
	*old_t = *new_t;
	return deltaT;
}

uint32_t Mov_Avg_handler(uint32_t* dataT[], int index,  uint32_t* avg)
{
    int sum = 0;
    int i;
	for(i = 0; i < index; i++)
	{
		sum+= *dataT[i];
	}

	*avg =(sum / index);

	return *avg;
}

uint32_t BPM_Calc_handler(uint32_t* avg, uint32_t* BPM)
{

	*BPM=60/(*avg);

 return *BPM;
 }

void Displayhandler(uint32_t* BPM)
{
 char msg[250];
 printf(msg, "BPM: %d\r\n", (int)BPM);
 HAL_UART_Transmit(&huart3, (uint8_t*) msg, strlen(msg), HAL_MAX_DELAY); //change uart #
}

switch(eNextState)
	  {
	   case PeakDetection_State:
		   
		   eNextState=TimeDiff_Calc_State;
	   break;

	   case TimeDiff_Calc_State:
	       utime = TimeDiffhandler(&old_t, &new_t);
	       dataT[index] = utime;
	       index++;

	       if(index > 5)
	       {
	        index=0;
	        sc=1;
	       }
	       if(sc==1)
	       {
	    	   eNextState = Moving_Avg_State;
	       }
	       else
	       {
	    	   eNextState = PeakDetection_State;
	       }

	   break;

	   case Moving_Avg_State:
	       Mov_Avg_handler(&dataT, index, &avg);
	       eNextState = BPM_Calc_State;

	   break;

	   case BPM_Calc_State:
	       BPM_Calc_handler(&avg, &BPM);
	       eNextState=Display_State;
	   break;

	   case Display_State:
	       Displayhandler(&BPM);
	       eNextState=PeakDetection_State;
	   break;

	   default:
	   Peakhandler();
	   break;

	   }
